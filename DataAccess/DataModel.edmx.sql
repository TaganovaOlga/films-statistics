
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/09/2017 22:30:01
-- Generated from EDMX file: C:\Users\AvGusta\Desktop\4dimka\SDZDB\DataAccess\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [KolyaTheKiller];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GamesCompany]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GamesSet] DROP CONSTRAINT [FK_GamesCompany];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CompanySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CompanySet];
GO
IF OBJECT_ID(N'[dbo].[GamesSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GamesSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'FilmSet'
CREATE TABLE [dbo].[FilmSet] (
    [Id] uniqueidentifier  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Country] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AboutSet'
CREATE TABLE [dbo].[AboutSet] (
    [Id] uniqueidentifier  NOT NULL,
    [Reziser] nvarchar(max)  NOT NULL,
    [Genre] nvarchar(max)  NOT NULL,
    [Studia] nvarchar(max)  NOT NULL,
    [Value] int  NOT NULL,
    [FilmId] uniqueidentifier  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'FilmSet'
ALTER TABLE [dbo].[FilmSet]
ADD CONSTRAINT [PK_FilmSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AboutSet'
ALTER TABLE [dbo].[AboutSet]
ADD CONSTRAINT [PK_AboutSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [FilmId] in table 'AboutSet'
ALTER TABLE [dbo].[AboutSet]
ADD CONSTRAINT [FK_GamesCompany]
    FOREIGN KEY ([FilmId])
    REFERENCES [dbo].[FilmSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GamesCompany'
CREATE INDEX [IX_FK_GamesCompany]
ON [dbo].[AboutSet]
    ([FilmId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------