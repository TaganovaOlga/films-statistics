﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataAccess;

namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для GameEditWindow.xaml
    /// </summary>
    public partial class GameEditWindow : Window
    {
        About Gam;
        private Film CompanyCopy;
        private DataModelContainer CompanyContain;

        public GameEditWindow(Film a, About b, DataModelContainer c, bool check)
        {
            InitializeComponent();
            CompanyCopy = a;
            CompanyContain = c;
            DataContext = this;
            if (check)
            {
                OkCommand = new SimpleCommand(Add, TrueMethod);
            }
            else
            {
                Gam = b;
                gameName.Text = Gam.Reziser.ToString();
                gameGenre.Text = Gam.Genre.ToString();
                gamePlatform.Text = Gam.Studia.ToString();
                gameValue.Text = Gam.Value.ToString();
                OkCommand = new SimpleCommand(Change, TrueMethod);
            }
        }
        private void Change()
        {
            Gam = CompanyContain.AboutSet.First(x => x.Id == Gam.Id);
            Gam.FilmId = CompanyCopy.Id;
            Gam.Reziser = gameName.Text;
            Gam.Genre = gameGenre.Text;
            Gam.Studia = gamePlatform.Text;
            Gam.Value = int.Parse(gameValue.Text); 
            CompanyContain.SaveChanges();
            DialogResult = false;
            Close();
        }

        private void Add()
        {
            About newGame = new About();
            newGame.Id = Guid.NewGuid();
            newGame.FilmId = CompanyCopy.Id;
            if (gameName.Text != "")
            {
                newGame.Reziser = gameName.Text;
            }
            else { gameName.Text = "Unknown"; newGame.Reziser = "Unknown"; }
            if (gameGenre.Text!= "")
            {
                newGame.Genre = gameGenre.Text;
            }
            else { gameGenre.Text = "Unknown"; newGame.Genre = "Unknown"; }
            if (gamePlatform.Text != "")
            {
                newGame.Studia = gamePlatform.Text;
            }
            else { gamePlatform.Text = "Unknown"; newGame.Studia = "Unknown"; }
            try
            {
                newGame.Value = int.Parse(gameValue.Text);
            }
            catch { }
            finally
            {
                if (newGame.Value == 0)
                {
                    gameValue.Text = "0";
                    newGame.Value = int.Parse(gameValue.Text);
                }
            }
            

            CompanyCopy.About.Add(newGame);
            CompanyContain.AboutSet.Add(newGame);
            CompanyContain.SaveChanges();
            DialogResult = false;
            this.Close();
        }
        public bool TrueMethod()
        {
            return true;
        }


        public SimpleCommand OkCommand
        {
            get { return (SimpleCommand)GetValue(OkCommandProperty); }
            set { SetValue(OkCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OkCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty OkCommandProperty =
            DependencyProperty.Register("OkCommand", typeof(SimpleCommand), typeof(GameEditWindow), new PropertyMetadata(null));




    }
}