﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DataAccess;

namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для CompanyEditWindow.xaml
    /// </summary>
    public partial class CompanyEditWindow : Window
    {
        private 
            Film CompanyCopy;
        private DataModelContainer container;
        public CompanyEditWindow(Film a, DataModelContainer b, bool check = true)
        {
            InitializeComponent();
            container = b;
            DataContext = this;
            CompanyCopy = a;
            if (check)
            {
                Complete = new SimpleCommand(Add, TrueMethod);
            }
            else
            {
                companyName.Text = a.Name.ToString();
                companyCountry.Text = a.Country.ToString();
                Complete = new SimpleCommand(Change, TrueMethod);
            }
        }
        private void Add()
        {
            Film newCompany = new Film();
            newCompany.Id = Guid.NewGuid();
            if (companyName.Text != "")
            {
                newCompany.Name = companyName.Text;
            }
            else { companyName.Text = "Unknown"; newCompany.Name = "Unknown"; }

            if (companyCountry.Text != "")
            {
                newCompany.Country = companyCountry.Text;
            }
            else { companyCountry.Text = "Unknown"; newCompany.Country = "Unknown"; ; }
            container.FilmSet.Add(newCompany);
            container.SaveChanges();
            DialogResult = false;
            Close();
        }
        private void Change()
        {
            CompanyCopy.Name = companyName.Text;
            CompanyCopy.Country = companyCountry.Text;
            container.SaveChanges();
            DialogResult = false;
            this.Close();
        }
        public bool TrueMethod()
        {
            return true;
        }
        public SimpleCommand Complete
        {
            get { return (SimpleCommand)GetValue(CompleteProperty); }
            set { SetValue(CompleteProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Complete.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CompleteProperty =
            DependencyProperty.Register("Complete", typeof(SimpleCommand), typeof(CompanyEditWindow), new PropertyMetadata(null));
    }
}
