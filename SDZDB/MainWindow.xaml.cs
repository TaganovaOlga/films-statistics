﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataAccess;
using System.Collections.ObjectModel;

namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataModelContainer _bdContainer = new DataModelContainer();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            AddCompanyPr = new SimpleCommand(AddCompany, TrueMethod);
            DeleteCompanyPr = new SimpleCommand(DeleteCompany, CheckCompany);
            EditCompanyPr = new SimpleCommand(EditCompany, CheckCompany);
            AddGamePr = new SimpleCommand(AddGame, CheckCompany);
            DeleteGamePr = new SimpleCommand(DeleteGame, CheckGames);
            EditGamePr = new SimpleCommand(EditGame, CheckGames);
            UpdateCompany();
            RefreshGames();
        }
        public void AddCompany()
        {
            CompanyEditWindow EditWindow = new CompanyEditWindow(ChooseCompanyPr, _bdContainer);
            EditWindow.ShowDialog();
            if (EditWindow.DialogResult == false)
            {
                UpdateCompany();
                RefreshGames();
            }
        }
        public void DeleteCompany()
        {
            try
            {
                List<About> GameList = new List<About>();
                foreach (var games in ChooseCompanyPr.About)
                {
                    GameList.Add(games);
                }
                foreach (var item in GameList)
                {
                    ChooseCompanyPr.About.Remove(item);
                    _bdContainer.AboutSet.Remove(item);
                    if (ChooseCompanyPr.About.Count == 0)
                    {
                        break;
                    }
                }
                foreach (var company in _bdContainer.FilmSet)
                {
                    if (company.Id == ChooseCompanyPr.Id)
                    {
                        _bdContainer.FilmSet.Remove(company);
                        break;
                    }
                }
                _bdContainer.SaveChanges();
                UpdateCompany();
                RefreshGames();
            }
            catch (Exception)
            {
            }
        }
        private void EditCompany()
        {
            CompanyEditWindow EditWindow = new CompanyEditWindow(ChooseCompanyPr, _bdContainer, false);
            EditWindow.ShowDialog();
            if (EditWindow.DialogResult == false)
            {
                UpdateCompany();
                RefreshGames();
            }
        }   
        public bool CheckCompany()
        {
            if (ChooseCompanyPr != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void UpdateCompany(bool flag = true)
        {
            if (flag)
            {
                _bdContainer = new DataModelContainer();
            }
            CompanyListPr = _bdContainer.FilmSet.ToList();
        }
        public void RefreshGames()
        {
            if (ChooseCompanyPr != null)
            {
                if (ChooseCompanyPr.About != null)
                {
                    GameListPr = new ObservableCollection<About>(ChooseCompanyPr.About);
                }
            }
            else
            {
                GameListPr = new ObservableCollection<About>();
            }
        }
        public bool TrueMethod()
        {
            return true;
        }
        private void AddGame()
        {
            GameEditWindow EditWindow = new GameEditWindow(ChooseCompanyPr, null, _bdContainer, true);
            EditWindow.ShowDialog();
            if (EditWindow.DialogResult == false)
            {
                RefreshGames();
            }
        }
        private void DeleteGame()
        {
            ChooseCompanyPr.About.Remove(ChooseGamePr);
            _bdContainer.AboutSet.Remove(ChooseGamePr);
            _bdContainer.SaveChanges();
            RefreshGames();
        }
        private void EditGame()
        {
            GameEditWindow EditWindow = new GameEditWindow(ChooseCompanyPr, ChooseGamePr, _bdContainer, false);
            EditWindow.ShowDialog();
            if (EditWindow.DialogResult == false)
            {
                RefreshGames();
            }

        }
        public bool CheckGames()
        {
            if (ChooseCompanyPr != null && ChooseGamePr != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }





        public SimpleCommand AddCompanyPr
        {
            get { return (SimpleCommand)GetValue(addCompanyPrProperty); }
            set { SetValue(addCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for addCompanyPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty addCompanyPrProperty =
            DependencyProperty.Register("AddCompanyPr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));

        public SimpleCommand DeleteCompanyPr
        {
            get { return (SimpleCommand)GetValue(DeleteCompanyPrProperty); }
            set { SetValue(DeleteCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for DeleteCompanyPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteCompanyPrProperty =
            DependencyProperty.Register("DeleteCompanyPr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));
        public SimpleCommand EditCompanyPr
        {
            get { return (SimpleCommand)GetValue(EditCompanyPrProperty); }
            set { SetValue(EditCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for EditCompanyPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EditCompanyPrProperty =
            DependencyProperty.Register("EditCompanyPr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));
        public SimpleCommand AddGamePr
        {
            get { return (SimpleCommand)GetValue(AddGamePrProperty); }
            set { SetValue(AddGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for AddGamePr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddGamePrProperty =
            DependencyProperty.Register("AddGamePr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));
        public SimpleCommand DeleteGamePr
        {
            get { return (SimpleCommand)GetValue(DeleteGamePrProperty); }
            set { SetValue(DeleteGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for DeleteGamePr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DeleteGamePrProperty =
            DependencyProperty.Register("DeleteGamePr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));
        public SimpleCommand EditGamePr
        {
            get { return (SimpleCommand)GetValue(EditGamePrProperty); }
            set { SetValue(EditGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for EditGamePr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EditGamePrProperty =
            DependencyProperty.Register("EditGamePr", typeof(SimpleCommand), typeof(MainWindow), new PropertyMetadata(null));
        public IEnumerable<Film> CompanyListPr
        {
            get { return (IEnumerable<Film>)GetValue(CompanyListPrProperty); }
            set { SetValue(CompanyListPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for CompanyListPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CompanyListPrProperty =
            DependencyProperty.Register("CompanyListPr", typeof(IEnumerable<Film>), typeof(MainWindow), new PropertyMetadata(null));
        public Film ChooseCompanyPr
        {
            get { return (Film)GetValue(ChooseCompanyPrProperty); }
            set { SetValue(ChooseCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ChooseCompanyPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChooseCompanyPrProperty =
            DependencyProperty.Register("ChooseCompanyPr", typeof(Film), typeof(MainWindow), new PropertyMetadata(null, SelCompany));
        public ObservableCollection<About> GameListPr
        {
            get { return (ObservableCollection<About>)GetValue(GameListPrProperty); }
            set { SetValue(GameListPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for GameListPr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GameListPrProperty =
            DependencyProperty.Register("GameListPr", typeof(ObservableCollection<About>), typeof(MainWindow), new PropertyMetadata(null));
        public About ChooseGamePr
        {
            get { return (About)GetValue(ChooseGamePrProperty); }
            set { SetValue(ChooseGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ChooseGamePr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ChooseGamePrProperty =
            DependencyProperty.Register("ChooseGamePr", typeof(About), typeof(MainWindow), new PropertyMetadata(null));
        private static void SelCompany(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var a = d as MainWindow;
                if (a != null)
                {
                    a.GameListPr = new ObservableCollection<About>(a.ChooseCompanyPr.About);
                }
            }
            catch (Exception)
            {}
        }

    }
}