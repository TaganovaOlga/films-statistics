﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace SDZDB
{
    public class SimpleCommand : ICommand
    {
        private Action _executeMethod;
        private Func<bool> _canExecuteMethod;

        public SimpleCommand(Action p_execute, Func<bool> p_canExexute = null)
        {
            _executeMethod = p_execute;
            _canExecuteMethod = p_canExexute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecuteMethod != null ? _canExecuteMethod() : false;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }

            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (_canExecuteMethod != null)
            {
                _executeMethod();
            }
        }
    }
}