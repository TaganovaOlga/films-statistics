﻿Public Class CompanyEditWindow

    End Classusing System;
Using System.Collections.Generic;
Using System.Linq;
Using System.Text;
Using System.Threading.Tasks;
Using System.Windows;
Using System.Windows.Controls;
Using System.Windows.Data;
Using System.Windows.Documents;
Using System.Windows.Input;
Using System.Windows.Media;
Using System.Windows.Media.Imaging;
Using System.Windows.Shapes;
Using DataAccess;

Namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для CompanyEditWindow.xaml
    /// </summary>
    Partial Public Class CompanyEditWindow :  Window
    {
        Private 
            Film CompanyCopy;
        Private DataModelContainer container;
        Public CompanyEditWindow(Film a, DataModelContainer b, bool check = True)
        {
            InitializeComponent();
            container = b;
            DataContext = this;
            CompanyCopy = a;
            If (check)
            {
                Complete = New SimpleCommand(Add, TrueMethod);
            }
            Else
            {
                companyName.Text = a.Name.ToString();
                companyCountry.Text = a.Country.ToString();
                Complete = New SimpleCommand(Change, TrueMethod);
            }
        }
        Private void Add()
        {
            Film newCompany = New Film();
            newCompany.Id = Guid.NewGuid();
            If (companyName.Text!= "")
            {
                newCompany.Name = companyName.Text;
            }
            Else { companyName.Text = "Unknown"; newCompany.Name = "Unknown"; }

            If (companyCountry.Text!= "")
            {
                newCompany.Country = companyCountry.Text;
            }
            Else { companyCountry.Text = "Unknown"; newCompany.Country = "Unknown"; ; }
            container.FilmSet.Add(newCompany);
            container.SaveChanges();
            DialogResult = false;
            Close();
        }
        Private void Change()
        {
            CompanyCopy.Name = companyName.Text;
            CompanyCopy.Country = companyCountry.Text;
            container.SaveChanges();
            DialogResult = false;
            this.Close();
        }
        Public bool TrueMethod()
        {
            Return True;
        }
        Public SimpleCommand Complete
        {
            Get { Return (SimpleCommand)GetValue(CompleteProperty); }
            Set { SetValue(CompleteProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Complete.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty CompleteProperty =
            DependencyProperty.Register("Complete", TypeOf(SimpleCommand), TypeOf(CompanyEditWindow), New PropertyMetadata(null));
    }
}
