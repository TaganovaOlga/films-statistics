﻿Using System;
Using System.Collections.Generic;
Using System.Linq;
Using System.Text;
Using System.Threading.Tasks;
Using System.Windows;
Using System.Windows.Controls;
Using System.Windows.Data;
Using System.Windows.Documents;
Using System.Windows.Input;
Using System.Windows.Media;
Using System.Windows.Media.Imaging;
Using System.Windows.Shapes;
Using DataAccess;

Namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для GameEditWindow.xaml
    /// </summary>
    Partial Public Class GameEditWindow :  Window
    {
        About Gam;
        Private Film CompanyCopy;
        Private DataModelContainer CompanyContain;

        Public GameEditWindow(Film a, About b, DataModelContainer c, bool check)
        {
            InitializeComponent();
            CompanyCopy = a;
            CompanyContain = c;
            DataContext = this;
            If (check)
            {
                OkCommand = New SimpleCommand(Add, TrueMethod);
            }
            Else
            {
                Gam = b;
                gameName.Text = Gam.Reziser.ToString();
                gameGenre.Text = Gam.Genre.ToString();
                gamePlatform.Text = Gam.Studia.ToString();
                gameValue.Text = Gam.Value.ToString();
                OkCommand = New SimpleCommand(Change, TrueMethod);
            }
        }
        Private void Change()
        {
            Gam = CompanyContain.AboutSet.First(x => x.Id == Gam.Id);
            Gam.FilmId = CompanyCopy.Id;
            Gam.Reziser = gameName.Text;
            Gam.Genre = gameGenre.Text;
            Gam.Studia = gamePlatform.Text;
            Gam.Value = int.Parse(gameValue.Text); 
            CompanyContain.SaveChanges();
            DialogResult = false;
            Close();
        }

        Private void Add()
        {
            About newGame = New About();
            newGame.Id = Guid.NewGuid();
            newGame.FilmId = CompanyCopy.Id;
            If (gameName.Text!= "")
            {
                newGame.Reziser = gameName.Text;
            }
            Else { gameName.Text = "Unknown"; newGame.Reziser = "Unknown"; }
            If (gameGenre.Text! = "")
            {
                newGame.Genre = gameGenre.Text;
            }
            Else { gameGenre.Text = "Unknown"; newGame.Genre = "Unknown"; }
            If (gamePlatform.Text!= "")
            {
                newGame.Studia = gamePlatform.Text;
            }
            Else { gamePlatform.Text = "Unknown"; newGame.Studia = "Unknown"; }
            Try
            {
                newGame.Value = int.Parse(gameValue.Text);
            }
            Catch { }
            Finally
            {
                If (newGame.Value == 0)
                {
                    gameValue.Text = "0";
                    newGame.Value = int.Parse(gameValue.Text);
                }
            }
            

            CompanyCopy.About.Add(newGame);
            CompanyContain.AboutSet.Add(newGame);
            CompanyContain.SaveChanges();
            DialogResult = false;
            this.Close();
        }
        Public bool TrueMethod()
        {
            Return True;
        }


        Public SimpleCommand OkCommand
        {
            Get { Return (SimpleCommand)GetValue(OkCommandProperty); }
            Set { SetValue(OkCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for OkCommand.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty OkCommandProperty =
            DependencyProperty.Register("OkCommand", TypeOf(SimpleCommand), TypeOf(GameEditWindow), New PropertyMetadata(null));




    }
}