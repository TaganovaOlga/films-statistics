﻿Using System;
Using System.Collections.Generic;
Using System.Linq;
Using System.Text;
Using System.Threading.Tasks;
Using System.Windows;
Using System.Windows.Controls;
Using System.Windows.Data;
Using System.Windows.Documents;
Using System.Windows.Input;
Using System.Windows.Media;
Using System.Windows.Media.Imaging;
Using System.Windows.Navigation;
Using System.Windows.Shapes;
Using DataAccess;
Using System.Collections.ObjectModel;

Namespace SDZDB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    Partial Public Class MainWindow :  Window
    {
        Private DataModelContainer _bdContainer = New DataModelContainer();
        Public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            AddCompanyPr = New SimpleCommand(AddCompany, TrueMethod);
            DeleteCompanyPr = New SimpleCommand(DeleteCompany, CheckCompany);
            EditCompanyPr = New SimpleCommand(EditCompany, CheckCompany);
            AddGamePr = New SimpleCommand(AddGame, CheckCompany);
            DeleteGamePr = New SimpleCommand(DeleteGame, CheckGames);
            EditGamePr = New SimpleCommand(EditGame, CheckGames);
            UpdateCompany();
            RefreshGames();
        }
        Public void AddCompany()
        {
            CompanyEditWindow EditWindow = New CompanyEditWindow(ChooseCompanyPr, _bdContainer);
            EditWindow.ShowDialog();
            If (EditWindow.DialogResult == False)
            {
                UpdateCompany();
                RefreshGames();
            }
        }
        Public void DeleteCompany()
        {
            Try
            {
                List<About> GameList = New List<About>();
                foreach (var games in ChooseCompanyPr.About)
                {
                    GameList.Add(games);
                }
                foreach (var item in GameList)
                {
                    ChooseCompanyPr.About.Remove(item);
                    _bdContainer.AboutSet.Remove(item);
                    If (ChooseCompanyPr.About.Count == 0)
                    {
                        break;
                    }
                }
                foreach (var company in _bdContainer.FilmSet)
                {
                    If (company.Id == ChooseCompanyPr.Id)
                    {
                        _bdContainer.FilmSet.Remove(company);
                        break;
                    }
                }
                _bdContainer.SaveChanges();
                UpdateCompany();
                RefreshGames();
            }
            Catch (Exception)
            {
            }
        }
        Private void EditCompany()
        {
            CompanyEditWindow EditWindow = New CompanyEditWindow(ChooseCompanyPr, _bdContainer, False);
            EditWindow.ShowDialog();
            If (EditWindow.DialogResult == False)
            {
                UpdateCompany();
                RefreshGames();
            }
        }   
        Public bool CheckCompany()
        {
            If (ChooseCompanyPr!= null)
            {
                Return True;
            }
            Else
            {
                Return False;
            }
        }
        Public void UpdateCompany(bool flag = True)
        {
            If (flag)
            {
                _bdContainer = New DataModelContainer();
            }
            CompanyListPr = _bdContainer.FilmSet.ToList();
        }
        Public void RefreshGames()
        {
            If (ChooseCompanyPr!= null)
            {
                If (ChooseCompanyPr.About!= null)
                {
                    GameListPr = New ObservableCollection<About>(ChooseCompanyPr.About);
                }
            }
            Else
            {
                GameListPr = New ObservableCollection<About>();
            }
        }
        Public bool TrueMethod()
        {
            Return True;
        }
        Private void AddGame()
        {
            GameEditWindow EditWindow = New GameEditWindow(ChooseCompanyPr, null, _bdContainer, True);
            EditWindow.ShowDialog();
            If (EditWindow.DialogResult == False)
            {
                RefreshGames();
            }
        }
        Private void DeleteGame()
        {
            ChooseCompanyPr.About.Remove(ChooseGamePr);
            _bdContainer.AboutSet.Remove(ChooseGamePr);
            _bdContainer.SaveChanges();
            RefreshGames();
        }
        Private void EditGame()
        {
            GameEditWindow EditWindow = New GameEditWindow(ChooseCompanyPr, ChooseGamePr, _bdContainer, False);
            EditWindow.ShowDialog();
            If (EditWindow.DialogResult == False)
            {
                RefreshGames();
            }

        }
        Public bool CheckGames()
        {
            If (ChooseCompanyPr!= null && ChooseGamePr!= null)
            {
                Return True;
            }
            Else
            {
                Return False;
            }
        }





        Public SimpleCommand AddCompanyPr
        {
            Get { Return (SimpleCommand)GetValue(addCompanyPrProperty); }
            Set { SetValue(addCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for addCompanyPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty addCompanyPrProperty =
            DependencyProperty.Register("AddCompanyPr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));

        Public SimpleCommand DeleteCompanyPr
        {
            Get { Return (SimpleCommand)GetValue(DeleteCompanyPrProperty); }
            Set { SetValue(DeleteCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for DeleteCompanyPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty DeleteCompanyPrProperty =
            DependencyProperty.Register("DeleteCompanyPr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));
        Public SimpleCommand EditCompanyPr
        {
            Get { Return (SimpleCommand)GetValue(EditCompanyPrProperty); }
            Set { SetValue(EditCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for EditCompanyPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty EditCompanyPrProperty =
            DependencyProperty.Register("EditCompanyPr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));
        Public SimpleCommand AddGamePr
        {
            Get { Return (SimpleCommand)GetValue(AddGamePrProperty); }
            Set { SetValue(AddGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for AddGamePr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty AddGamePrProperty =
            DependencyProperty.Register("AddGamePr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));
        Public SimpleCommand DeleteGamePr
        {
            Get { Return (SimpleCommand)GetValue(DeleteGamePrProperty); }
            Set { SetValue(DeleteGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for DeleteGamePr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty DeleteGamePrProperty =
            DependencyProperty.Register("DeleteGamePr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));
        Public SimpleCommand EditGamePr
        {
            Get { Return (SimpleCommand)GetValue(EditGamePrProperty); }
            Set { SetValue(EditGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for EditGamePr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty EditGamePrProperty =
            DependencyProperty.Register("EditGamePr", TypeOf(SimpleCommand), TypeOf(MainWindow), New PropertyMetadata(null));
        Public IEnumerable<Film> CompanyListPr
        {
            Get { Return (IEnumerable<Film>)GetValue(CompanyListPrProperty); }
            Set { SetValue(CompanyListPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for CompanyListPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty CompanyListPrProperty =
            DependencyProperty.Register("CompanyListPr", TypeOf(IEnumerable<Film>), TypeOf(MainWindow), New PropertyMetadata(null));
        Public Film ChooseCompanyPr
        {
            Get { Return (Film)GetValue(ChooseCompanyPrProperty); }
            Set { SetValue(ChooseCompanyPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ChooseCompanyPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty ChooseCompanyPrProperty =
            DependencyProperty.Register("ChooseCompanyPr", TypeOf(Film), TypeOf(MainWindow), New PropertyMetadata(null, SelCompany));
        Public ObservableCollection<About> GameListPr
        {
            Get { Return (ObservableCollection<About>)GetValue(GameListPrProperty); }
            Set { SetValue(GameListPrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for GameListPr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty GameListPrProperty =
            DependencyProperty.Register("GameListPr", TypeOf(ObservableCollection<About>), TypeOf(MainWindow), New PropertyMetadata(null));
        Public About ChooseGamePr
        {
            Get { Return (About)GetValue(ChooseGamePrProperty); }
            Set { SetValue(ChooseGamePrProperty, value); }
        }
        // Using a DependencyProperty as the backing store for ChooseGamePr.  This enables animation, styling, binding, etc...
        Public Static ReadOnly DependencyProperty ChooseGamePrProperty =
            DependencyProperty.Register("ChooseGamePr", TypeOf(About), TypeOf(MainWindow), New PropertyMetadata(null));
        Private Static void SelCompany(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Try
            {
                var a = d As MainWindow;
                If (a!= null)
                {
                    a.GameListPr = New ObservableCollection<About>(a.ChooseCompanyPr.About);
                }
            }
            Catch (Exception)
            {}
        }

    }
}